'use strict';

/* App Module */

var placesApp = angular.module('placesApp', [
  'ngRoute',
  'angular-loading-bar',
  'ngAnimate',
  'placesControllers',
  'placesServices'
]);

placesApp.config(['$routeProvider',
function($routeProvider) {
  $routeProvider.
  when('/places', {
    redirectTo: '/'
  }).
  when('/#myCarousel', {
  }).
  when('/', {
    templateUrl: 'partials/places-list.html',
    controller: 'PlacesListCtrl'
  }).
  when('/:placeId', {
    templateUrl: 'partials/place-detail.html',
    controller: 'PlaceDetailCtrl'
  }).
  when('/:placeId/edit', {
    templateUrl: 'partials/place-edit.html',
    controller: 'PlaceEditCtrl'
  }).
  when('/add', {
    templateUrl: 'partials/place-edit.html',
    controller: 'PlaceAddCtrl'
  }).
  otherwise({
    redirectTo: '/'
  });
}]);
