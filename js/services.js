'use strict';

/* Services */

var placesServices = angular.module('placesServices', ['ngResource']);

placesServices.factory('Places', ['$resource',
  function($resource){
    return $resource('https://places-marchrius.rhcloud.com/:name', {}, {
      query: { method:'GET', isArray:true},
      get: { method: 'GET', isArray:false},
    });
  }]);
