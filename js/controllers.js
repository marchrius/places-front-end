'use strict';

/* Controllers */

var placesControllers = angular.module('placesControllers', []);

placesControllers.controller('PlacesListCtrl', ['$scope', 'Places',
function($scope, Places) {
  Places.query({name: "places"}, function(data){
    $scope.places = data;
    $scope.showError = false;
  },function(error) {
    console.log("Errore: " + error);
    $scope.error = 'Code "'+error.status+'". ' + error.statusText;
    $scope.showError = true;
  });
  $scope.orderProp = 'name';

  $scope.downloadImage = function(place) {
    var link = document.createElement('a');
    link.download = place.name + ".png";
    link.href = 'data:application/octet-stream;base64,' + place.image;
    link.click();
  };

  $scope.editPlace = function (place) {
    var link = document.createElement('a');
    link.href = '#/' + place.id + '/edit';
    link.click();
  };
}]);

placesControllers.controller('PlaceDetailCtrl', ['$scope', '$routeParams', 'Places',
function($scope, $routeParams, Places) {
  Places.get({name: $routeParams.placeId}, function(data) {
    $scope.place = data;
    $scope.showError = false;
  },function(error) {
    console.log("Errore: " + error.statusText);
    $scope.error = 'Code "'+error.status+'". ' + error.statusText;
    $scope.showError = true;
  });

}]);

placesControllers.controller('PlaceEditCtrl', ['$scope', '$routeParams', '$http', 'Places',
function($scope, $routeParams, $http, Places) {
  Places.get({name: $routeParams.placeId}, function(data) {
    $scope.place = data;
    $scope.nameField = data.name;
    $scope.languageField = data.language;
    $scope.residentField = data.resident;
    $scope.imageField = 'data:image/png;base64,' + data.image;
    $scope.showError = false;
  },function(error) {
    console.log("Errore: " + error);
    $scope.error = 'Code "' + error.status + '". ' + error.statusText;
    $scope.showError = true;
  });

  $scope.editPlace = function () {
    $scope.newPlace = $scope.place;
    $scope.newPlace.name = $scope.name;
    $scope.newPlace.language = $scope.language;
    $scope.newPlace.resident = $scope.resident;

    if ($scope.newImage != null) {
      $scope.newPlace.image = $scope.newImage.replace(/^data:image\/[^;]+;base64,/, "");
    }

    var request = {
      method: 'POST',
      url: 'https://places-marchrius.rhcloud.com/update/edit',
      headers: {
        'Access-Control-Allow-Origin' : '*'
      },
      data: $scope.place
    }

    $scope.success = true;

    $http(request, $scope.place,
      function (success) {},
      function (error) {
        $scope.success = false;
      }
    );
  };

  $scope.loadPreview = function(element) {
    $scope.file = element.files[0];
    var reader = new FileReader();

    reader.onload = function(event) {
      $scope.newImage = event.target.result;
      $scope.$apply();
    };
    reader.readAsDataURL($scope.file);
  };
}]);

placesControllers.controller('PlaceAddCtrl', ['$scope', '$http',
function ($scope, $http) {
  $scope.oldImage.style = "display: none";
  $scope.addPlace = function () {
    $scope.newPlace = {name: "", language: "", resident: 0, image: ""};
    $scope.newPlace.name = $scope.name;
    $scope.newPlace.language = $scope.language;
    $scope.newPlace.resident = $scope.resident;

    if ($scope.newImage != null) {
      $scope.newPlace.image = $scope.newImage.replace(/^data:image\/[^;]+;base64,/, "");
    }

    var request = {
      method: 'POST',
      url: 'https://places-marchrius.rhcloud.com/update/add',
      headers: {
        'Access-Control-Allow-Origin' : '*'
      },
      data: $scope.place
    }

    $scope.success = true;

    $http(request, $scope.place,
      function (success) {},
      function (error) {
        $scope.success = false;
      }
    );
  };
  $scope.loadPreview = function(element) {
    $scope.file = element.files[0];
    var reader = new FileReader();

    reader.onload = function(event) {
      $scope.newImage = event.target.result;
      $scope.$apply();
    };
    reader.readAsDataURL($scope.file);
  };
}]);
